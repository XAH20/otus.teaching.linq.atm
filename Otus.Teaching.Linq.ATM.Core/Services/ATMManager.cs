﻿using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.Linq.ATM.Core.Entities;

namespace Otus.Teaching.Linq.ATM.Core.Services
{
    public class ATMManager
    {
        public IEnumerable<Account> Accounts { get; private set; }

        public IEnumerable<User> Users { get; private set; }

        public IEnumerable<OperationsHistory> History { get; private set; }

        public ATMManager(IEnumerable<Account> accounts, IEnumerable<User> users,
            IEnumerable<OperationsHistory> history)
        {
            Accounts = accounts;
            Users = users;
            History = history;
        }

        public User GetUser(string login, string password)
            => Users.FirstOrDefault(u => u.Login == login && u.Password == password);

        public List<Account> GetAccounts(int userId)
            => Accounts.Where(a => a.UserId == userId).ToList();

        public IEnumerable<object> GetAccountsWithHistory(int userId)
        {
            return
                from account in Accounts
                where account.UserId == userId
                join history in History on account.Id equals history.AccountId 
                orderby account.Id
                select new 
                {
                    AccountId=account.Id, 
                    account.OpeningDate, 
                    account.CashAll, 
                    OperationsHistoryId = history.Id, 
                    history.OperationDate, 
                    history.OperationType, 
                    history.CashSum};
        }

        public IEnumerable<object> GetOperationsInputCash()
        {
            return
                from history in History
                where history.OperationType == OperationType.InputCash
                join account in Accounts on history.AccountId equals account.Id
                join user in Users on account.UserId equals user.Id
                orderby account.Id
                select new
                {
                    OperationsHistoryId = history.Id,
                    history.OperationDate,
                    history.OperationType,
                    history.CashSum,
                    AccountId = account.Id,
                    UserId = user.Id,
                    user.FirstName,
                    user.SurName
                };
        }

        public IEnumerable<User> GetUsersWithCashGreater(decimal minCash)
        {
            return
                (from account in Accounts
                where account.CashAll > minCash
                join user in Users on account.UserId equals user.Id
                orderby user.Id
                select user)
                .Distinct();
        }
    }
}