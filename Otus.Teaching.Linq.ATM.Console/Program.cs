﻿using System.Linq;
using Otus.Teaching.Linq.ATM.Core.Services;
using Otus.Teaching.Linq.ATM.DataAccess;

namespace Otus.Teaching.Linq.ATM.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine("Старт приложения-банкомата...");

            var atmManager = CreateATMManager();

            System.Console.WriteLine("------------------------");
            ShowUserInfo(atmManager, "snow", "111");
            System.Console.WriteLine("------------------------");
            ShowUserInfo(atmManager, "snow", "222");
            System.Console.WriteLine("------------------------");
            ShowAccountsUserInfo(atmManager, 1);
            System.Console.WriteLine("------------------------");
            ShowAccountsUserInfo(atmManager, 3);
            System.Console.WriteLine("------------------------");
            ShowAccountsUserInfo(atmManager, 10);
            System.Console.WriteLine("------------------------");
            ShowAccountsUserInfoWithHistory(atmManager, 1);
            System.Console.WriteLine("------------------------");
            ShowAccountsUserInfoWithHistory(atmManager, 5);
            System.Console.WriteLine("------------------------");
            ShowAccountsUserInfoWithHistory(atmManager, 50);
            System.Console.WriteLine("------------------------");
            ShowOperationsInputCash(atmManager);
            System.Console.WriteLine("------------------------");
            ShowUsersWithCashGreater(atmManager, 10000m);
            System.Console.WriteLine("------------------------");
            ShowUsersWithCashGreater(atmManager, 10000000m);
            System.Console.WriteLine("------------------------");

            System.Console.WriteLine("Завершение работы приложения-банкомата...");
        }

        static ATMManager CreateATMManager()
        {
            using var dataContext = new ATMDataContext();
            var users = dataContext.Users.ToList();
            var accounts = dataContext.Accounts.ToList();
            var history = dataContext.History.ToList();

            return new ATMManager(accounts, users, history);
        }

        static void ShowUserInfo(ATMManager atmManager, string login, string password)
        {
            System.Console.WriteLine($"Информация о пользователе с логином \"{login}\" и паролем \"{password}\": ");
            var user = atmManager.GetUser(login, password);
            System.Console.WriteLine(user != null
                ? user.ToString()
                : "Пользователь с данной парой логин/пароль отстутствует в базе");
        }

        static void ShowAccountsUserInfo(ATMManager atmManager, int id)
        {
            System.Console.WriteLine($"Информация о счетах пользователя с id={id}:");
            var accounts = atmManager.GetAccounts(id);
            if (accounts.Count == 0)
            {
                System.Console.WriteLine("Пользователь с заданным id отстутствует в базе или у заданного пользователя нет счетов");
            }
            else
            {
                foreach (var account in accounts)
                {
                    System.Console.WriteLine(account);
                }
            }
        }

        static void ShowAccountsUserInfoWithHistory(ATMManager atmManager, int id)
        {
            System.Console.WriteLine($"Информация о счетах пользователя с id={id}:");
            var accountsWithHistory = atmManager.GetAccountsWithHistory(id);
            var accountsWithHistoryList = accountsWithHistory.ToList();
            if (accountsWithHistoryList.Count == 0)
            {
                System.Console.WriteLine("Пользователь с заданным id отстутствует в базе или у заданного пользователя нет счетов");
            }
            else
            {
                foreach (var accountWithHistory in accountsWithHistoryList)
                {
                    System.Console.WriteLine(accountWithHistory);
                }
            }
        }

        static void ShowOperationsInputCash(ATMManager atmManager)
        {
            System.Console.WriteLine("Информация о всех операциях пополенения счёта с указанием владельца каждого счёта");
            var operations = atmManager.GetOperationsInputCash();
            var operationsList = operations.ToList();
            if (operationsList.Count == 0)
            {
                System.Console.WriteLine("Операции пополенения счёта отстутствуют в базе");
            }
            else
            {
                foreach (var operation in operationsList)
                {
                    System.Console.WriteLine(operation);
                }
            }
        }

        static void ShowUsersWithCashGreater (ATMManager atmManager, decimal minCash)
        {
            System.Console.WriteLine($"Информация о пользователях, у которых на счёте сумма больше {minCash}:");

            var users = atmManager.GetUsersWithCashGreater(minCash);
            var usersList = users.ToList();
            if (usersList.Count == 0)
            {
                System.Console.WriteLine($"Пользователи, у которых на счёте сумма больше {minCash}, отстутствуют в базе");
            }
            else
            {
                foreach (var user in usersList)
                {
                    System.Console.WriteLine(user);
                }
            }
        }
    }
}